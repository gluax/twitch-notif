#!/bin/sh

dir="$(dirname "$0")"
. "$dir/common"

installed "curl"
installed "jq"
expect_n_args_debug 3 "Expected Usage: ./clean_gitlab <gitlabe_project_id> <gitlab_project_token> <number_of_days_pipeline_live>" "$@"

PROJECT=$1
TOKEN=$2
NUMBER_OF_DAYS=$3

sc=$(curl --request GET \
	  --write-out "%{http_code}" --silent --output /dev/null \
	  --url "https://gitlab.com/api/v4/projects/${PROJECT}/jobs?per_page=100" \
	  --header "private-token: $TOKEN")
if [ "$sc" -ne 200 ] ; then
    error "Bad Status Code $sc."
    info "INFO: Check your Gitlab Project Id and Gitlab Project Token"
    exit 1
fi

positive_intp "$NUMBER_OF_DAYS" "3rd argument"

# Keep the 3 most recent runs of the pipeline. 
JOBS=$(curl --request GET --fail \
	    --url "https://gitlab.com/api/v4/projects/${PROJECT}/jobs?per_page=100" \
	    --header "private-token: ${TOKEN}" \
	   | jq -r 'unique_by(.pipeline.id) | .[] | [.pipeline.created_at, .pipeline.id] | join(",")' | sort -r | sed '1,3d')

for PIPELINE_INFO in $JOBS; do
    timestamp=$(echo "$PIPELINE_INFO" | cut -d ',' -f1)
    pid=$(echo "$PIPELINE_INFO" | cut -d ',' -f2)
    days_old=$(time_diff_in_days "$timestamp")

    if [ "$days_old" -gt "$NUMBER_OF_DAYS" ]; then
	curl --request DELETE --fail \
	     --url "https://gitlab.com/api/v4/projects/${PROJECT}/pipelines/${pid}" \
	     --header "private-token: ${TOKEN}"
    fi
done
