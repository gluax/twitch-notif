pub type TopicHandler = fn();

pub struct WebSocketTopic {
    pub name: String,
    pub connected: bool,
    pub auth_token: String,
    pub hash: String,
    pub nonce: Option<String>,
    pub handler: TopicHandler,
}

impl WebSocketTopic {
    pub fn new(name: String, auth_token: String, handler: TopicHandler) -> WebSocketTopic {
        WebSocketTopic {
            name: name.clone(),
            connected: false,
            auth_token: auth_token.clone(),
            nonce: None,
            hash: format!("{}:{}", name, auth_token),
            handler,
        }
    }
}
