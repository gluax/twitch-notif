use bytes::Bytes;
use crate::topic::WebSocketTopic;
use chrono::prelude::{DateTime, Utc};
use std::thread;
use std::sync::{Arc, Mutex, mpsc::channel, mpsc::Receiver, mpsc::Sender};
use tokio::net::TcpStream;
use tokio::time::{Duration, delay_for};
use futures::stream::StreamExt;
use tokio_tungstenite::{connect_async, WebSocketStream};

pub type LogFn = fn(&Connection);

pub struct Connection {
    pub host: String,
    pub auth_token: Option<String>,

    pub connection: Option<WebSocketStream<TcpStream>>,
    pub connected: Arc<Mutex<bool>>,
    pub reconnect: bool,

    pub writer_stop_sender: Sender<bool>,
    pub writer_stop_receiver: Receiver<bool>,
    pub writer: (Sender<Bytes>, Receiver<Bytes>),

    pub reader_stop_sender: Sender<bool>,
    pub reader_stop_receiver: Receiver<bool>,
    pub reader: (Sender<Bytes>, Receiver<Bytes>),

    pub last_pong: Arc<Mutex<DateTime<Utc>>>,

    pub topics: Vec<WebSocketTopic>,
    pub log_fn: LogFn,
}

impl Connection {
    pub fn new(host: String, token: Option<String>, log_fn: LogFn) -> Connection {
        let (writer_stop_sender, writer_stop_receiver) = channel::<bool>();
        let (reader_stop_sender, reader_stop_receiver) = channel::<bool>();
        Connection {
            host,
            auth_token: token,

            connection: None,
            connected: Arc::new(Mutex::new(false)),
            reconnect: true,

            writer_stop_sender,
            writer_stop_receiver,
            writer: channel::<Bytes>(),

            reader_stop_sender,
            reader_stop_receiver,
            reader: channel::<Bytes>(),

            last_pong: Arc::new(Mutex::new(Utc::now())),
            
            topics: Vec::new(),
            log_fn,
        }
    }

    pub fn set_auth_token(&mut self, token: Option<String>) {
        self.auth_token = token;
    }

    pub async fn start_reader(&mut self) {
        // let conn = self.connection.as_mut().expect("Cannot start reader without connection");
        // let (_, mut read) = conn.split();

        let (sender, _) = &self.reader;
        let reader = sender.clone();
        let rss = self.reader_stop_sender.clone();

        thread::spawn(move || {
            
            loop {
                // read.next();
                rss.send(true);
                reader.send(Bytes::from("Hello World."));

            }
        });
    }

    pub fn log(&self) {
        (self.log_fn)(&self)
    }

    pub fn on_pong(&mut self) {
        let mut data = self.last_pong.lock().unwrap();
        *data = Utc::now();
    }

    pub fn last_pong_within_limits(&self, time: DateTime<Utc>) -> bool {
        let data = self.last_pong.lock().unwrap();

        // config settings for time since last poing
        time.signed_duration_since(*data) < chrono::Duration::from_std(Duration::from_secs(10)).unwrap()
    }

    pub async fn ping(&mut self) {
        let time = Utc::now();

        // config settings for pong deadline
        delay_for(Duration::from_secs(10)).await;
        if !self.last_pong_within_limits(time) {
            self.reconnect = true;
            self.on_disconnect().await;
        }

    }

    pub async fn start_ping(&self) {
        // config settings for last ping
        delay_for(Duration::from_secs(240)).await;
    }

    pub fn set_connected(&mut self) {
        let mut data = self.connected.lock().unwrap();
        *data = !(*data);
    }

    pub fn is_connected(&self) -> bool {
        let data = self.connected.lock().unwrap();

        *data
    }

    pub async fn connect(&mut self) {
        let (conn, _) = connect_async(&self.host).await.expect("Failed to connect.");
        self.connection = Some(conn);
        
        self.set_connected();
        self.start_ping().await;
    }

    pub async fn disconnect(&mut self) {
        match self.connection.as_mut() {
            Some(conn) => {
                conn.close(None).await.expect("Error closing connection.");
            }
            None => {}
        }
    }

    pub async fn on_disconnect(&mut self) {
        if !*self.connected.lock().unwrap() {
            return
        }

        // config setting for reconect time
        delay_for(Duration::from_secs(5)).await;
        self.connect().await;
    }

    pub fn num_topics(&self) -> usize {
        self.topics.len()
    }
}
