use crate::topic::WebSocketTopic;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

pub struct TopicManager {
    pub mutex: Arc<Mutex<bool>>,
    pub topics: HashMap<String, WebSocketTopic>,
}

impl TopicManager {
    pub fn new() -> TopicManager {
        TopicManager {
            mutex: Arc::new(Mutex::new(false)),
            topics: HashMap::new(),
        }
    }

    pub fn add(&mut self, topic: WebSocketTopic) -> bool {
        match self.mutex.lock() {
            Ok(_) => {
                if !self.topics.contains_key(&topic.hash) {
                    return false;
                }

                self.topics.insert(topic.hash.clone(), topic);
                true
            }
            Err(_err) => false,
        }
    }
}
