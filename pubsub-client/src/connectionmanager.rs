use crate::connection::{Connection, LogFn};
use crate::topic::WebSocketTopic;
use std::sync::{Arc, Mutex};

pub struct ConnectionManager {
    pub host: String,

    pub connections_mutex: Arc<Mutex<bool>>,
    pub connections: Vec<Connection>,

    pub connection_limit_mutex: Arc<Mutex<bool>>,
    connection_limit: i32,

    pub topic_limit_mutex: Arc<Mutex<bool>>,
    topic_limit: i32,
}

impl ConnectionManager {
    pub fn new(host: String) -> ConnectionManager {
        ConnectionManager {
            host,

            connections_mutex: Arc::new(Mutex::new(false)),
            connections: Vec::new(),

            connection_limit_mutex: Arc::new(Mutex::new(false)),
            connection_limit: 10,

            topic_limit_mutex: Arc::new(Mutex::new(false)),
            topic_limit: 10,
        }
    }

    pub fn set_connection_limit(&mut self, limit: i32) {
        match self.connection_limit_mutex.lock() {
            Ok(_) => self.connection_limit = limit,
            Err(_err) => {}
        }
    }

    pub fn get_connection_limit(&self) -> i32 {
        match self.connection_limit_mutex.lock() {
            Ok(_) => self.connection_limit,
            Err(_err) => 0,
        }
    }

    pub fn set_topic_limit(&mut self, limit: i32) {
        match self.topic_limit_mutex.lock() {
            Ok(_) => self.topic_limit = limit,
            Err(_err) => {}
        }
    }

    pub fn get_topic_limit(&self) -> i32 {
        match self.topic_limit_mutex.lock() {
            Ok(_) => self.topic_limit,
            Err(_err) => 0,
        }
    }

    pub fn run(&self) {}

    pub fn refresh_topic(&mut self, _topic: WebSocketTopic, log_fn: LogFn) {
        let topic_limit = self.get_topic_limit();

        for conn in self.connections.iter() {
            if conn.num_topics() >= topic_limit as usize {
                continue;
            }

            return;
        }

        if self.connections.len() < self.get_connection_limit() as usize {
            let _conn = self.add_connection(log_fn);
            return;
        }
    }

    pub fn add_connection(&mut self, log_fn: LogFn) -> Connection {
        let conn = Connection::new(self.host.clone(), None, log_fn);
        conn
    }

    pub async fn disconnect(&mut self) {
        for conn in self.connections.iter_mut() {
            if !conn.is_connected() {
                return;
            }

            conn.disconnect().await;
        }
    }
}
